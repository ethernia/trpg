#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ----------------------
#    Character Select
# ----------------------

func_gm2_char_select () {
    clear
    DEBUG="0"
    if [ "$LANGUAGE" == "EN" ]; then
        echo "» Select your Character «"
        echo ""
        echo "1  »  Warrior  «(default)"
        echo "2  »  Assasin"
        echo ""
    elif [ "$LANGUAGE" == "DE" ]; then
        echo "» Wähle deinen Charakter «"
        echo ""
        echo "1  »  Krieger  «(standard)"
        echo "2  »  Assasine"
        echo ""
    fi

    read -n 1 -r -p "[$HOSTNAME » $USER]»»» " PROCESS
    if [ "$PROCESS" == "1" ]; then
        TYPE="WARRIOR"
    elif [ "$PROCESS" == "2" ]; then
        TYPE="ASSASIN"
    else
        TYPE="WARRIOR"
    fi
}