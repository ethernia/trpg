#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------------------------
#    Debug Functions (W.I.P)
# -----------------------------

# Creates Directory with Record File
func_debug_gamesave_setup () {
    mkdir -p ~/.trpg
    cd ~/.trpg
    touch .trpg-sequence-record
    RECORD="0"
}
# Removes Game Folder (Not Game Files, just Save Files)
func_debug_gamesave_remove () {
    cd ~
    rm -rf ~/.trpg/
    clear
    exit
}

# Disposable Input Function
func_input_check () {
    echo ""
    read -n 1 -r -p "[$USERNAME]»»» " INPUT
    echo ""
}

# Variable Checker, put your Variables here and call the Function whenever you need to check a Variable!
func_debug_check_full () {
    if [ "$DEBUG" == "1" ]; then
        if [ "$LANGUAGE" == "EN" ]; then
            echo "[DEBUG]» Debug Mode enabled! [DEBUG = $DEBUG]"
            echo "[DEBUG]» Name: [$USERNAME]"
            echo "[DEBUG]» Input: [$INPUT]"
            echo "[DEBUG]| Variables:"
            echo "       | [PROCESS = $SEQUENCE @ $PROCESS]"
            echo "       | [Player Health = $PLAYER_VAL1_HEALTH]"
            echo "       | [Player XP = $PLAYER_VAL1_XP]"
            echo "       | [Player Damage Output = $PLAYER_VAL1_DMG]"
            echo "       | [Player Stunned Status = $PLAYER_VAL1_STUNNED]"
            echo "       | [Mud Crawler Val1 Health = $MUDCRAWLER_VAL1_HEALTH]"
            echo "       | [Mud Crawler Val1 XP Output = $MUDCRAWLER_VAL1_XPOUT]"
            echo "       | [Mud Crawler Val1 Damage Output = $MUDCRAWLER_VAL1_DMG]"
            echo "       | [Mud Crawler Val1 Stunned Status = $MUDCRAWLER_VAL1_STUNNED]"
            echo "[DEBUG]» Loaded:"
            echo "         PLAYER VAL1: $PLAYER_VAL1_LOADED"
            echo "         MUDCRAWLER VAL1: $MUDCRAWLER_VAL1_LOADED"
            echo ""
            echo "#"
            echo ""
        elif [ "$LANGUAGE" == "DE" ]; then
            echo "[DEBUG]» Debug-Modus aktiviert! [DEBUG = $DEBUG]"
            echo "[DEBUG]» Name: [$USERNAME]"
            echo "[DEBUG]» Input: [$INPUT]"
            echo "[DEBUG]| Variablen:"
            echo "       | [PROZESS = $SEQUENCE @ $PROCESS]"
            echo "       | [Spieler Leben = $PLAYER_VAL1_HEALTH]"
            echo "       | [Spieler Erfahrung = $PLAYER_VAL1_XP]"
            echo "       | [Spieler Schadenzufuhr = $PLAYER_VAL1_DMG]"
            echo "       | [Player Schock Status = $PLAYER_VAL1_STUNNED]"
            echo "       | [Mud Crawler Val1 Leben = $MUDCRAWLER_VAL1_HEALTH]"
            echo "       | [Mud Crawler Val1 Ehrfahrung = $MUDCRAWLER_VAL1_XPOUT]"
            echo "       | [Mud Crawler Val1 Schadenzufuhr = $MUDCRAWLER_VAL1_DMG]"
            echo "       | [Mud Crawler Val1 Schock Status = $MUDCRAWLER_VAL1_STUNNED]"
            echo "[DEBUG]» Loaded:"
            echo "         Spieler Wert1: $PLAYER_VAL1_LOADED"
            echo "         Mud Crawler Wert1: $MUDCRAWLER_VAL1_LOADED"
            echo ""
            echo "#"
            echo ""
        fi
    fi
}

# Stats Saver
func_debug_stats_save () {
    if (( $SEQUENCE >= $RECORD )); then
        rm -rf .trpg-sequence-record
        touch .trpg-sequence-record
        echo "$SEQUENCE" >> .trpg-sequence-record
    fi
}

# Stats Caller (Exits & Saves)
func_debug_stats_checker_saveexit () {
    if (( $SEQUENCE >= $RECORD )); then
        rm -rf .trpg-sequence-record
        touch .trpg-sequence-record
        echo "$SEQUENCE" >> .trpg-sequence-record
    fi

    if [ "$LANGUAGE" == "EN" ]; then
        echo ""
        echo " Stats:"
        echo "+------+"
        echo "Sequence:"
        echo "$SEQUENCE"
        echo "Record: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» PRESS ANY KEY TO EXIT « (Your Record will be saved!)"
        exit
    elif [ "$LANGUAGE" == "DE" ]; then
        echo ""
        echo " Statistiken:"
        echo "+------------+"
        echo "Sequenz:"
        echo "$SEQUENCE"
        echo "Rekord: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» DRÜCKE EINE TASTE ZUM BEENDEN « (Dein Rekord wird gespeichert!)"
        exit
    fi
}

# Stats Caller (Exits)
func_debug_stats_checker_exit () {
    if [ "$LANGUAGE" == "EN" ]; then
        echo ""
        echo " Stats:"
        echo "+------+"
        echo "Sequence:"
        echo "$SEQUENCE"
        echo "Record: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» PRESS ANY KEY TO EXIT « (Your Record will NOT be saved!)"
        exit
    elif [ "$LANGUAGE" == "DE" ]; then
        echo ""
        echo " Statistiken:"
        echo "+------------+"
        echo "Sequenz:"
        echo "$SEQUENCE"
        echo "Rekord: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» DRÜCKE EINE TASTE ZUM BEENDEN « (Dein Rekord wird NICHT gespeichert!)"
        exit
    fi
}

# Stats Caller (Saves)
func_debug_stats_checker_save () {
    if (( $SEQUENCE >= $RECORD )); then
        rm -rf .trpg-sequence-record
        touch .trpg-sequence-record
        echo "$SEQUENCE" >> .trpg-sequence-record
    fi

    if [ "$LANGUAGE" == "EN" ]; then
        echo ""
        echo " Stats:"
        echo "+------+"
        echo "Sequence:"
        echo "$SEQUENCE"
        echo "Record: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» PRESS ANY KEY TO CONTINUE « (Your Record will be saved!)"
    elif [ "$LANGUAGE" == "DE" ]; then
        echo ""
        echo " Statistiken:"
        echo "+------------+"
        echo "Sequenz:"
        echo "$SEQUENCE"
        echo "Rekord: "
        cat .trpg-sequence-record
        echo "" && sleep 2
        read -n 1 -r -p "» DRÜCKE EINE TASTE ZUM BEENDEN « (Dein Rekord wird gespeichert!)"
        exit
    fi
}