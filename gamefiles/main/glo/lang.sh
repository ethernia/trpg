#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ---------------------
#    Language Select
# ---------------------

func_lang_select () {
    clear
    DEBUG="0"
    echo "1  »  TRPG English  «(default)"
    echo "2  »  TRPG Deutsch"
    echo ""
    echo "3  »  TRPG English  [DEBUG]"
    echo "4  »  TRPG Deutsch  [DEBUG]"
    echo ""
    echo "0  »  Stop"
    echo "X  »  REMOVE GAME SAVES (Records, etc.)"
    echo ""
    read -n 1 -r -p "[$HOSTNAME » $USER]»»» " PROCESS
    if [ "$PROCESS" == "1" ]; then
        LANGUAGE="EN"
        func_title_screen
    elif [ "$PROCESS" == "2" ]; then
        LANGUAGE="DE"
        func_title_screen
    elif [ "$PROCESS" == "3" ]; then
        DEBUG="1"
        LANGUAGE="EN"
        func_title_screen
    elif [ "$PROCESS" == "4" ]; then
        DEBUG="1"
        LANGUAGE="DE"
        func_title_screen
    elif [ "$PROCESS" == "0" ]; then
        clear
        exit
    elif [ "$PROCESS" == "X" ]; then
        func_debug_gamesave_remove
    else
        LANGUAGE="EN"
        func_title_screen
    fi

    func_gm1    #   ««««« Change this to change your Game Mode Function!
}