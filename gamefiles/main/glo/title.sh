#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------
#    Title
# -----------

func_title_screen () {
    clear
    if [ "$LANGUAGE" == "EN" ]; then
        echo "+-------------------------+"
        echo "|    made by Ethernia     |"
        echo "| ethernia-root.github.io |"
        echo "+-------------------------+"
        echo "| GNU GPL v3              |"
        echo "| (c) 2022                |"
        echo "+-------------------------+"
        echo ""
        echo "   TRPG TEST  ·  v.2.0.0"
        echo ""
        echo "Welcome to TestRPG!"
        echo "TRPG is a simple and easily moddable Text RPG written in Bash."
        echo "There will be no Wiki, if you know how to Bash-Script, you can mod this game!"
        echo ""
        echo "Debug Mode just displays some Variables and Information about everything."
        echo "It might let you cheat and is otherwise pretty useless for a player and is meant for testing!"
        echo "(for Developers, Modders, Bug Fixing, etc.)"
        echo ""
        echo "Choose your Game Mode:"
        echo "1  »  Testing World (Demo Game)      «(default)"
        echo "2  »  Example World (Snippets *!Experimental!*)"
        echo ""
        read -n 1 -r -p "[$HOSTNAME » $USER]»»» " GAMEMODE
    elif [ "$LANGUAGE" == "DE" ]; then
        echo "+-------------------------+"
        echo "|    made by Ethernia     |"
        echo "| ethernia-root.github.io |"
        echo "+-------------------------+"
        echo "| GNU GPL v3              |"
        echo "| (c) 2022                |"
        echo "+-------------------------+"
        echo ""
        echo "   TRPG TEST  ·  v.2.0.0"
        echo ""
        echo "Willkommen bei TestRPG!"
        echo "TRPG ist ein simples und leicht modifizierbares Text-RPG geschrieben in Bash."
        echo "Es wird kein Wiki geben, wenn du weißt, wie man Bash-Skriptet, kannst du dieses Spiel modden!"
        echo ""
        echo "Der Debug Modus zeigt nur ein paar Variablen und Informationen über alles an."
        echo "Es kann dich vielleicht schummeln lassen und ist ansonsten ganz schön nutzlos für einen Spieler, es ist nur zum Testen gedacht!"
        echo "(für Entwickler, Modder, Bug Fixen, etc.)"
        echo ""
        echo "Wähle deinen Spielmodus:"
        echo "1  »  Testing World (Demo Game)      «(standard)"
        echo "2  »  Example World (Snippets *!Experimentell!*)"
        echo ""
        read -n 1 -r -p "[$HOSTNAME » $USER]»»» " GAMEMODE
    fi

    if [ "$GAMEMODE" == "1" ]; then
        func_gm1
    elif [ "$GAMEMODE" == "2" ]; then
        func_gm2
    else
        func_gm1
    fi
}