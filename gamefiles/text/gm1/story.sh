#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------
#    Story
# -----------

# Story Functions are Scenes that can happen at any Time related to the Story.
# For example:
# - Death
# - Victory

# Death
func_gm1_story_death () {
    clear
    if [ "$LANGUAGE" == "EN" ]; then
        echo "»»» CONGRATULATIONS! YOU DIED! «««"
        echo "»»» RESULTS:    Loading...     «««"
        echo "" && sleep 3
        echo "[Test Dummy]> ..." && sleep 2
        echo "[Test Dummy]> You are a disgrace to your Family..." && sleep 1.5
        echo "[Test Dummy]> You only managed to reach Sequence $SEQUENCE..." && sleep 2
        echo "[Test Dummy]> You did not pass the test." && sleep 1
        echo "[Test Dummy]> Initiating Sequence C..." && sleep 1
        echo "..." && sleep 1
        echo "..." && sleep 1
        echo "..." && sleep 1.25
        echo "*mechanical sound*" && sleep 1
        echo "[Test Dummy]> Executing Process 2 of Sequence C..." && sleep 1
        echo "..." && sleep 1
        echo ""
        echo "You loose Consciousness."
        echo "You died." && sleep 1
    elif [ "$LANGUAGE" == "DE" ]; then
        echo "»»» GLÜCKWUNSCH! DU BIST GESTORBEN! «««"
        echo "»»» ERGENBIS:    Lädt . . .         «««"
        echo "" && sleep 3
        echo "[Test Dummy]> ..." && sleep 2
        echo "[Test Dummy]> Du bist eine Enttäuschung für deine Familie..." && sleep 1.5
        echo "[Test Dummy]> Du hast es nur bis zu Sequenz $SEQUENCE geschafft..." && sleep 2
        echo "[Test Dummy]> Du hast den Test nicht bestanden..." && sleep 1
        echo "[Test Dummy]> Initialisiere Sequenz C..." && sleep 1
        echo "..." && sleep 1
        echo "..." && sleep 1
        echo "..." && sleep 1.25
        echo "*mechanischer Ton*" && sleep 1
        echo "[Test Dummy]> Führe Prozess 2 der Sequenz C aus..." && sleep 1
        echo "..." && sleep 1
        echo ""
        echo "Du verlierst dein Bewusstsein."
        echo "Du bist gestorben." && sleep 1
    fi
    func_debug_stats_checker_saveexit
}

# Victory
func_gm1_story_victory () {
    #clear « COMMENTED BECAUSE USELESS ATM
    if [ "$LANGUAGE" == "EN" ]; then
        echo "»»» CONGRATULATIONS! You won! «««"
        echo "»»» RESULTS:    Loading...    «««"
    elif [ "$LANGUAGE" == "DE" ]; then
        echo "»»» GLÜCKWUNSCH! Du hast gewnonen! «««"
        echo "»»» ERGENBIS:    Lädt . . .        «««"
    fi
    func_debug_stats_checker_saveexit
}