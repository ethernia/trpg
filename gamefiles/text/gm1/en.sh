#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------------
#    Game Mode 1
#        EN
# -----------------

func_gm1_en_text_s0_1 () {
    echo "Welcome to the World of Testing!"
    echo "" && sleep 3
    echo "[Test Dummy]> Hello!" && sleep 1
    echo "[Test Dummy]> This is the first Test Sequence. Please enter your Name!"
    read -r -p "[YOU]»»» " USERNAME
    echo "[Test Dummy]> Greetings, $USERNAME! In the first Sequence, we will test your Strength." && sleep 4
    echo "[Test Dummy]> I will spawn in a Mud Crawler. Your Goal is to defeat the Mud Crawler without taking too much Damage!" && sleep 6
    echo "[Test Dummy]> Good Luck." && sleep 1
    echo ""
    read -n 1 -r -p "» PRESS ANY KEY TO START SEQUENCE 1 «"
}

func_gm1_en_text_s1_1 () {
    echo "A Mud Crawler spawned!"
    echo ""
    echo "1  »  Attack the Mud Crawler using your Fists «(default)"
    echo "2  »  Search for a Weapon"
    echo "3  »  Scream for Help"
    echo "4  »  Run away"
}

func_gm1_en_text_s1_i1 () {
    echo "You attacked the Mud Crawler."
    echo ""
}
func_gm1_en_text_s1_i2 () {
    echo "You searched for a Weapon, but the World around you looks bright and Empty." && sleep 3.5
    echo "\"Where am I?\", you ask yourself." && sleep 2
    echo ""
    echo "While you were distracted, the Mud Crawler took its Chance to attack you."
    echo ""
}
func_gm1_en_text_s1_i3 () {
    echo "You scream for help." && sleep 1
    echo "It seems to have scared the Mud Crawler." && sleep 3
    echo "[Test Dummy]> N o o n e   c a n   h e a r   y o u . . ." && sleep 3.5
    echo ""
    echo "It seems there is nobody around you. You take your chance at attacking the Mud Crawler."
    echo ""
}
func_gm1_en_text_s1_i4 () {
    echo "While running away, the Mud Crawler jumped infront of you and attacked you." && sleep 5
    echo "You fell to the Ground and hit your Neck." && sleep 3
    echo ""
}

func_gm1_en_text_s2_1 () {
    echo "[Test Dummy]> Congratulations." && sleep 1.5
    echo "[Test Dummy]> You managed to defeat the Mud Crawler." && sleep 2.5
    echo "[Test Dummy]> Moving on to Sequence 2..." && sleep 2.5
    echo "[Test Dummy]> Oh! Your Health is only at $PLAYER_VAL1_HEALTH..." && sleep 4
    echo "[Test Dummy]> Let me heal you real quick. But dont expect that after every Sequence!" && sleep 7
}
func_gm1_en_text_s2_2 () {
    echo "[Test Dummy]> There you go!" && sleep 1
    echo "[Test Dummy]> Initia..." && sleep 1
    echo "[Test Dummy]> ...ting..." && sleep 1
    echo "[Test Dummy]> ...Seq-" && sleep 2
    echo "..." && sleep 2
    echo "..." && sleep 2
    echo "..." && sleep 2
    echo "It seems the Test Dummy broke." && sleep 3
    echo "That can only mean one thing..." && sleep 3
    echo "It means... it means..." && sleep 2
    echo "THE GAME IS NOT FINISHED!" && sleep .5
    echo "(what a horror!)" && sleep 4.5
    echo ""
    echo "But not to worry! You can finish it yourself!"
    echo "Its just Bash-Scripting, its pretty easy!"
    echo "Reading and understanding this Script should help you make your own Game out of this Preset!"
    echo "" && sleep 10
}