#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------------
#    Game Mode 1
#        DE
# -----------------

func_gm1_de_text_s0_1 () {
    echo "Willkommen in der Welt des Testens!"
    echo "" && sleep 3
    echo "[Test Dummy]> Hallo!" && sleep 1
    echo "[Test Dummy]> Dies ist die erste Testsequenz! Bitte gib deinen Namen ein!"
    read -r -p "[DU]»»» " USERNAME
    echo "[Test Dummy]> Sei gegrüßt, $USERNAME! In der ersten Sequenz, werden wir deine Stärke prüfen." && sleep 4
    echo "[Test Dummy]> Ich werde einen Mud Crawler erscheinen lassen. Dein Ziel ist es, den Mud Crawler zu besiegen, ohne zu viel Schaden einzustecken!" && sleep 6
    echo "[Test Dummy]> Viel Glück." && sleep 1
    echo ""
    read -n 1 -r -p "» DRÜCKE EINE TASTE UM SEQUENZ 1 ZU STARTEN «"
}

func_gm1_de_text_s1_1 () {
    echo "Ein Mud Crawler ist erschienen!"
    echo ""
    echo "1  »  Greife den Mud Crawler mit deinen Fäusten an «(standard)"
    echo "2  »  Suche nach einer Waffe"
    echo "3  »  Ruf nach Hilfe"
    echo "4  »  Renn weg"
}

func_gm1_de_text_s1_i1 () {
    echo "Du greifst den Mud Crawler an."
    echo ""
}
func_gm1_de_text_s1_i2 () {
    echo "Du hast nach einer Waffe gesucht, doch die Welt um dich sieht hell und leer aus." && sleep 3.5
    echo "\"Wo bin ich?\", fragst du dich." && sleep 2
    echo ""
    echo "Während du abgelenkt warst, ergriff der Mud Crawler die Chance dich anzugreifen."
    echo ""
}
func_gm1_de_text_s1_i3 () {
    echo "Du rufst nach Hilfe." && sleep 1
    echo "Es sieht so aus als wäre der Mud Crawler erschreckt." && sleep 3
    echo "[Test Dummy]> N i e m a n d   k a n n   d i c h   h ö r e n . . ." && sleep 3.5
    echo ""
    echo "Niemand scheint in deiner Nähe zu sein. Du ergreifst die Chance den Mud Crawler anzugreifen."
    echo ""
}
func_gm1_de_text_s1_i4 () {
    echo "Während du weggerannt bist, sprang der Mud Crawler vor dich." && sleep 5
    echo "Du bist auf den Boden gefallen und hast dir das Genick gebrochen." && sleep 3
    echo ""
}

func_gm1_de_text_s2_1 () {
    echo "[Test Dummy]> Glückwunsch." && sleep 1.5
    echo "[Test Dummy]> Du hast es geschafft den Mud Crawler zu besiegen." && sleep 2.5
    echo "[Test Dummy]> Weiter gehts zu Sequenz 2..." && sleep 2.5
    echo "[Test Dummy]> Oh! Deine Leben sind nur noch $PLAYER_VAL1_HEALTH..." && sleep 4
    echo "[Test Dummy]> Lass mich dich schnell heilen. Aber erwarte das nicht nach jeder Sequenz!" && sleep 7
}
func_gm1_de_text_s2_2 () {
    echo "[Test Dummy]> Hier, bitte!" && sleep 1
    echo "[Test Dummy]> Initia..." && sleep 1
    echo "[Test Dummy]> ...lisiere..." && sleep 1
    echo "[Test Dummy]> ...Seq-" && sleep 2
    echo "..." && sleep 2
    echo "..." && sleep 2
    echo "..." && sleep 2
    echo "Es scheint als wäre der Test Dummy kaputt." && sleep 3
    echo "Das kann nur eins bedeuten..." && sleep 3
    echo "Es bedeutet... es bedeutet..." && sleep 2
    echo "DAS SPIEL IST NICHT FERTIG!" && sleep .5
    echo "(was ein horror!)" && sleep 4.5
    echo ""
    echo "Aber keine Angst! Du kannst es selbst fertigstellen!"
    echo "Es ist nur Bash-Skripten, Es ist ganz schön einfach!"
    echo "Dieses Skript zu lesen und zu verstehen sollte dir dabei helfen, dein eigenes Spiel aus dieser Vorlage zu machen!"
    echo "" && sleep 10
}