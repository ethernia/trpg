#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ------------
#    Fights
# ------------

# NOTE:
# The Mud Crawler attacks first
# Its easy to randomize this or set up Items / a Variable that controls who attacks first
# But I didnt.
# And I dont care.
# Please do.
# Also the Fight doesnt actually output anything until the fight was calculated,
# if you implement Items or a different fighting system than letting the numbers do its thing,
# WHICH I HOPE YOU DO,
# please Implement that. Pro Tipp: use "echo" - its a cool command!

func_gm1_calc_fights_mudcrawler_val1_fight_start () {
    while (( $MUDCRAWLER_VAL1_HEALTH >= 0 )) && (( $PLAYER_VAL1_HEALTH >= 0 )); do
        if [ "$MUDCRAWLER_VAL1_STUNNED" == "0" ]; then
            PLAYER_VAL1_HEALTH_TEMP=$(expr $PLAYER_VAL1_HEALTH - $MUDCRAWLER_VAL1_DMG)
            PLAYER_VAL1_HEALTH="$PLAYER_VAL1_HEALTH_TEMP"
        else
            MUDCRAWLER_VAL1_STUNNED="0"
        fi

        if [ "$PLAYER_VAL1_STUNNED" == "0" ]; then
            MUDCRAWLER_VAL1_HEALTH_TEMP=$(expr $MUDCRAWLER_VAL1_HEALTH - $PLAYER_VAL1_DMG)
            MUDCRAWLER_VAL1_HEALTH="$MUDCRAWLER_VAL1_HEALTH_TEMP"          
        else
            PLAYER_VAL1_STUNNED="0"
        fi

        if (( $MUDCRAWLER_VAL1_HEALTH <= 0 )); then
            MUDCRAWLER_VAL1_LOADED="0"
        fi
        if (( $PLAYER_VAL1_HEALTH <= 0 )); then
            PLAYER_VAL1_LOADED="0"
            func_story_death
        fi
    done
}