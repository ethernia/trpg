#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# -----------------
#    Mud Crawler
# -----------------

func_gm1_game_monster_mudcrawler_val1_load () {
    MUDCRAWLER_VAL1_LOADED="1"
}
func_gm1_game_monster_mudcrawler_val1_unload () {
    MUDCRAWLER_VAL1_LOADED="0"
}

func_gm1_game_monster_mudcrawler_val1_stats_reset () {
    MUDCRAWLER_VAL1_HEALTH="14"
    MUDCRAWLER_VAL1_XPOUT="12"
    MUDCRAWLER_VAL1_DMG="2"
    MUDCRAWLER_VAL1_STUNNED="0"
}