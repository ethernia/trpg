#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ----------------
#       Room
#    Sequence 2
# ----------------

func_room_sequence2_main () {
    SEQUENCE="2"
    if [ "$LANGUAGE" == "EN" ]; then
        func_gm1_en_text_s2_1
    elif [ "$LANGUAGE" == "DE" ]; then
        func_gm1_de_text_s2_1
    fi
    func_game_player_player_val1_stats_reset
    if [ "$LANGUAGE" == "EN" ]; then
        func_gm1_en_text_s2_2
    elif [ "$LANGUAGE" == "DE" ]; then
        func_gm1_de_text_s2_2
    fi
    func_gm1_story_victory
}