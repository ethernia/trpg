#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ----------------
#       Room
#    Sequence 0
# ----------------

func_room_sequence0_main () {
    SEQUENCE="0"
    clear
    func_gm1_game_player_player_val1_load
    
    if [ "$LANGUAGE" == "EN" ]; then
        func_gm1_en_text_s0_1
    elif [ "$LANGUAGE" == "DE" ]; then
        func_gm1_de_text_s0_1
    fi
}