#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ----------------
#       Room
#    Sequence 1
# ----------------

func_room_sequence1_main () {
    SEQUENCE="1"
    clear
    func_gm1_game_monster_mudcrawler_val1_load
    func_gm1_game_player_player_val1_stats_reset
    func_gm1_game_monster_mudcrawler_val1_stats_reset

    if [ "$LANGUAGE" == "EN" ]; then
        func_gm1_en_text_s1_1
    elif [ "$LANGUAGE" == "DE" ]; then
        func_gm1_de_text_s1_1
    fi
    func_input_check
    
    if [ "$INPUT" == "1" ]; then
        if [ "$LANGUAGE" == "EN" ]; then
            func_gm1_en_text_s1_i1
        elif [ "$LANGUAGE" == "DE" ]; then
            func_gm1_de_text_s1_i1
        fi
        func_gm1_calc_fights_mudcrawler_val1_fight_start

    elif [ "$INPUT" == "2" ]; then
        if [ "$LANGUAGE" == "EN" ]; then
            func_gm1_en_text_s1_i2
        elif [ "$LANGUAGE" == "DE" ]; then
            func_gm1_de_text_s1_i2
        fi
        PLAYER_VAL1_STUNNED="1"
        func_gm1_calc_fights_mudcrawler_val1_fight_start

    elif [ "$INPUT" == "3" ]; then
        if [ "$LANGUAGE" == "EN" ]; then
            func_gm1_en_text_s1_i3
        elif [ "$LANGUAGE" == "DE" ]; then
            func_gm1_de_text_s1_i3
        fi
        MUDCRAWLER_VAL1_STUNNED="1"
        func_gm1_calc_fights_mudcrawler_val1_fight_start

    elif [ "$INPUT" == "4" ]; then
        if [ "$LANGUAGE" == "EN" ]; then
            func_gm1_en_text_s1_i4
        elif [ "$LANGUAGE" == "DE" ]; then
            func_gm1_de_text_s1_i4
        fi
        func_gm1_story_death
    else
        func_gm1_calc_fights_mudcrawler_val1_fight_start
    fi
}