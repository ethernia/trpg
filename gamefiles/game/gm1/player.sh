#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# ------------
#    Player
# ------------

func_gm1_game_player_player_val1_load () {
    PLAYER_VAL1_LOADED="1"
}
func_gm1_game_player_player_val1_unload () {
    PLAYER_VAL1_LOADED="0"
}

func_gm1_game_player_player_val1_stats_reset () {
    PLAYER_VAL1_HEALTH="100"
    PLAYER_VAL1_XP="0"
    PLAYER_VAL1_DMG="7"
    PLAYER_VAL1_STUNNED="0"
}