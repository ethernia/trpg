# TRPG
```
	Support:
	You can get Help in the README File of the Repository and it's Issue Page, or you can...
	Contact Me:
	ethernia-root.github.io
```
TRPG is an easily modifiable Text RPG...
You can't play it... but you can mod it!

# Version
v.2.0.0

# License
TPRG is published under the GNU General Public License v3.
A Copy of the GPL can be found in this Repository!
Feel free to make your own game out if this base and reupload it!

# To-Do
Finish the Game (for now, its just a Demo)

# Documentation
I wont publish documentation.
If you can bash-script, you can definitely mod this game!
But I might explain everything the script does here!
