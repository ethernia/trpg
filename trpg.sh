#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

# Global
source ./gamefiles/main/glo/debug.sh            # Main / Debug Functions
source ./gamefiles/main/glo/title.sh            # Main / Title Screen
source ./gamefiles/main/glo/lang.sh             # Main / Language Select

# Game Mode 1
source ./gamefiles/game/gm1/player.sh           # Game / Player
source ./gamefiles/game/gm1/mud-crawler.sh      # Game / Mud Crawler (Monster)
source ./gamefiles/game/gm1/room/sequence0.sh   # Room / Sequence 0
source ./gamefiles/game/gm1/room/sequence1.sh   # Room / Sequence 1
source ./gamefiles/calc/gm1/fights.sh           # Calc / Fights
source ./gamefiles/text/gm1/story.sh            # Text / Story
source ./gamefiles/text/gm1/en.sh               # Text / Game Mode 1 (EN)
source ./gamefiles/text/gm1/de.sh               # Text / Game Mode 1 (DE)
# Game Mode 2
source ./gamefiles/main/gm2/char.sh             # Main / Character Select

func_debug_gamesave_setup

# -----------------
#    Game Mode 1
#   Testing World
# -----------------
func_gm1 () {
    func_room_sequence0_main
    func_room_sequence1_main
    func_room_sequence2_main
}
# -----------------
#    Game Mode 2
#   Example World
# -----------------
func_gm2 () {
    func_gm2_char_select
}

func_lang_select
