#!/bin/bash
# +-------------------------+
# |    made by Ethernia     |
# | ethernia-root.github.io |
# +-------------------------+
# | GNU GPL v3              |
# | (c) 2022                |
# +-------------------------+

clear
echo "This is an example Modfile. DO NOT RUN THIS SCRIPT!"
echo "Comment the \"exit\" to make the Script work."
echo "Make sure it's in the Same Directory as \"trpg.sh\""

exit # Comment this to make the Script work!

clear
echo "This Mod creates a Function that sets a Variable in the Player File for Game Mode 1"
read -n 1 -r -p "» PRESS ANY KEY TO INSTALL THE MOD «"

clear
echo "Installing Mod..."
# Mod Player File
cd ./gamefiles/game/gm1/
echo "func_gm1_game_player_player_val1_mod_function () {" >> ./player.sh
echo "  MODVARIABLE=\"1\"" >> ./player.sh 
echo "}" >> ./player.sh

echo "Mod installed!"
